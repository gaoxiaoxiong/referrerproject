# ReferrerProject

#### 依赖配置
```
    implementation 'com.yolo:arch-storage:1.0.4'
    implementation "com.yolo:arch-http:1.0.0"
    implementation "com.yolo:yolo-repository-http:1.1.1"
    implementation 'com.yolo:arch-log:1.0.4'
    implementation "com.facebook.conceal:conceal:1.1.3@aar"

    implementation "com.squareup.okhttp3:okhttp:4.9.3"
    implementation "com.squareup.okhttp3:okhttp-urlconnection:4.9.3"
    implementation "com.squareup.okhttp3:logging-interceptor:4.9.3"
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:1.6.0"

    implementation 'com.appsflyer:af-android-sdk:6.11.1'
    implementation "com.android.installreferrer:installreferrer:2.2"
    implementation "com.google.code.gson:gson:2.9.0"
    implementation "com.google.firebase:firebase-analytics:21.0.0"

    implementation "com.blankj:utilcodex:1.31.0"
```

#### 初始化
```
InstallReferrerAttributionManager.Build()
            .setApplication(this)
            .setAppsflyerKey("123456avcfv")
            .setAuthorizationHmacMd5Key("SO\$jq*kSmpNUdOuc")
            .setAliasXTokenKey(R.raw.neomoe_business)
            .setAliasCommonXTokenKey(R.raw.neomoe_common)
            .setBusinessHost("https://freecall.abtalkapp.net")
            .setCommonHost("https://freecall.abtalkapp.net")
            .setAliasFbRawKey(R.raw.neomoe_common)
            .builder()
```
