package com.yolo.referrerporject

import android.app.Application
import com.yolo.referrerlibrary.InstallReferrerAttributionManager
import com.yolo.referrerlibrary.R

class BaseApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        InstallReferrerAttributionManager.Build()
            .setApplication(this)
            .setAppsflyerKey("123456avcfv")
            .setAuthorizationHmacMd5Key("SO\$jq*kSmpNUdOuc")
            .setAliasXTokenKey(R.raw.neomoe_business)
            .setAliasCommonXTokenKey(R.raw.neomoe_common)
            .setBusinessHost("https://freecall.abtalkapp.net")
            .setCommonHost("https://freecall.abtalkapp.net")
            .setAliasFbRawKey(R.raw.neomoe_common)
            .builder()
    }
}