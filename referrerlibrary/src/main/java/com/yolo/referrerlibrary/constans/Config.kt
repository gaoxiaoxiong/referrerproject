package com.yolo.referrerlibrary.constans

import com.yolo.cache.storage.CacheConstants

class Config {
    companion object{
        // 渠道
        val channel: String ="gp"

        const val KEY_SPACE_SP_HOME = "sp_key_home_"
        const val KET_SPACE_FILE_INSTALL = "file_key_install_"

        const val KEY_REPORT_INSTALL_CALL_SERVER_SUCCESSFUL = KEY_SPACE_SP_HOME + "report_install_call_successful"
        const val KEY_USER_GP_REFERRER_UTM_SOURCE: String = CacheConstants.KET_SPACE_FILE_INSTALL + "gp_referrer_utm_source"
        const val KEY_USER_INSTALL_INFO = KET_SPACE_FILE_INSTALL + "info"


        //接口网络请求地址
        const val API_NET_FIRST_OPEN = "/api/first_open/"
    }
}