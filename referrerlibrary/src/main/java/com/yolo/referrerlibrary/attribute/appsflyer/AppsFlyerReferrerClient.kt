package com.yolo.base.installl.attributes.appsflyer

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.yolo.base.installl.attributes.inter.OnGetInstallGoogleInfoBeanListener
import com.yolo.cache.storage.BuildConfig
import com.yolo.referrerlibrary.attribute.appsflyer.adapter.AdwordsAppsflyerAttributionAdapter
import com.yolo.referrerlibrary.attribute.appsflyer.adapter.DefaultAppsflyerAttributionAdapter
import com.yolo.referrerlibrary.attribute.appsflyer.adapter.FacebookAppsflyerAttributionAdapter
import com.yolo.referrerlibrary.base.AbsAttributes
import com.yolo.referrerlibrary.bean.InstallInfoBean


/**
  * @date 创建时间: 2023/7/20
  * @auther gxx
  * @description 处理AppsFlyerLib
 * @param appsflyerKey key参数
  **/
class AppsFlyerReferrerClient(val mApplication: Application,val appsflyerKey:String, val mOnGetInstallGoogleInfoBeanListener: OnGetInstallGoogleInfoBeanListener): AbsAttributes(mApplication),
    AppsFlyerConversionListener {
    private val TAG = "AppsFlyer"

    init {
        val appsflyer = AppsFlyerLib.getInstance()
        appsflyer.setDebugLog(BuildConfig.DEBUG)
        appsflyer.init(appsflyerKey,this,mApplication)
        appsflyer.start(mApplication)
    }

    companion object{
        const val AF_STATUS = "af_status"
        const val NON_ORGANIC = "Non-organic"//非自然用户量
    }

    var mInstallAppsflyerInfoBean: InstallInfoBean? = null
        private set

    private var mFacebookAppsflyerAttributionAdapter: FacebookAppsflyerAttributionAdapter = FacebookAppsflyerAttributionAdapter()//faceBook
    private var mAdwordsAppsflyerAttributionAdapter: AdwordsAppsflyerAttributionAdapter = AdwordsAppsflyerAttributionAdapter()//广告渠道来源
    private var mDefaultAppsflyerAttributionAdapter: DefaultAppsflyerAttributionAdapter = DefaultAppsflyerAttributionAdapter()//默认渠道来源

    override fun onConversionDataSuccess(conversionDataMap: MutableMap<String, Any>?) {
        if (conversionDataMap == null){
            mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
            return
        }

        val status = conversionDataMap[AF_STATUS]?:""
        if (status == NON_ORGANIC){//非自然用户量
            if (mFacebookAppsflyerAttributionAdapter.isFacebookChannel(conversionDataMap)){//faceBook渠道
                this.mInstallAppsflyerInfoBean = mFacebookAppsflyerAttributionAdapter.attributionAppsflyerCreator(conversionDataMap);
            }else if (mAdwordsAppsflyerAttributionAdapter.isAdwordsChannel(conversionDataMap)){//广告来的渠道
                this.mInstallAppsflyerInfoBean = mAdwordsAppsflyerAttributionAdapter.attributionAppsflyerCreator(conversionDataMap);
            }else{//默认渠道
                this.mInstallAppsflyerInfoBean = mDefaultAppsflyerAttributionAdapter.attributionAppsflyerCreator(conversionDataMap)
            }
            this.mInstallAppsflyerInfoBean?.gpReferrerUrl = mOnGetInstallGoogleInfoBeanListener.onGetInstallGoogleInfoBean()?.gpReferrerUrl?:""
            //通知出去
            mOnAttributesListener?.onAttributes(this::class.java, LISTENER_STATUS_WAIT)
        }else{
            if (mOnGetInstallGoogleInfoBeanListener.onGetInstallGoogleInfoBean() == null){
                return
            }
            val stringBuilder = StringBuilder()
            for (key in conversionDataMap.keys) {
                stringBuilder.append(key)
                stringBuilder.append("=")
                stringBuilder.append(conversionDataMap[key])
                stringBuilder.append("&")
            }
            val referrerUrl = stringBuilder.toString()
            mOnGetInstallGoogleInfoBeanListener.onGetInstallGoogleInfoBean()?.let {
                if (it.utmSource.isEmpty()){
                    it.utmSource = conversionDataMap[AF_STATUS] as String
                }
                it.infoSource= it.infoSource + "_af"
                it.afReferrer = referrerUrl
                //通知出去
                mOnAttributesListener?.onAttributes(this::class.java, LISTENER_STATUS_WAIT)
            }
        }
    }

    override fun onConversionDataFail(errorMessage: String?) {
        mOnAttributesListener?.onAttributes(this::class.java, LISTENER_STATUS_WAIT)
        if(BuildConfig.DEBUG){
          Log.d(TAG, "error getting conversion data: $errorMessage");
        }
    }

    override fun onAppOpenAttribution(p0: MutableMap<String, String>?) {
        mOnAttributesListener?.onAttributes(this::class.java, LISTENER_STATUS_WAIT)
        if(BuildConfig.DEBUG){
            Log.d(TAG, "onAppOpenAttribution: This is fake call.")
        }
    }

    override fun onAttributionFailure(errorMessage: String?) {
        if(BuildConfig.DEBUG){
            Log.d(TAG, "error onAttributionFailure : $errorMessage")
        }
        mOnGetInstallGoogleInfoBeanListener.onGetInstallGoogleInfoBean()?.let {
            it.infoSource = it.infoSource + "_af"
            it.afReferrer = "onAttributionFailure=$errorMessage"
            mOnAttributesListener?.onAttributes(this::class.java, LISTENER_STATUS_WAIT)
        }
    }



}