package com.yolo.referrerlibrary.attribute

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.yolo.base.installl.attributes.appsflyer.AppsFlyerReferrerClient
import com.yolo.base.installl.attributes.util.ReferrerUrlUtil
import com.yolo.base.installl.attributes.util.SaveInfoUtil
import com.yolo.cache.storage.CacheConstants
import com.yolo.cache.storage.YoloCacheStorage
import com.yolo.referrerlibrary.attribute.google.GoogleReferrerClient
import com.yolo.referrerlibrary.base.AbsAttributes
import com.yolo.referrerlibrary.bean.ForYoadxInstallInfoBean
import com.yolo.referrerlibrary.bean.InstallInfoBean
import com.yolo.referrerlibrary.bean.InstallParamsRequest
import com.yolo.referrerlibrary.report.FireBaseReport
import com.yolo.referrerlibrary.report.YoloReport

/**
  * @date 创建时间: 2023/7/21
  * @auther gxx
  * @description 合并归因
  **/
class MergeAttribute(val mGoogleReferrerClient:GoogleReferrerClient,val mAppsFlyerReferrerClient: AppsFlyerReferrerClient,val mApplication: Application,val mOnIsReportServiceSuccessListener:OnIsReportServiceSuccessListener) : AbsAttributes.OnAttributesListener {
    private var mAppReportUtils = FireBaseReport()//fireBase的上报
    private var mYoloReport = YoloReport()//yolo的上报
    private val mStatusMap = HashMap<Class<*>, Int>() //存储 ReferrerClient 与  AppsFlyer

    public interface OnIsReportServiceSuccessListener{
        fun isReportServiceSuccess():Boolean
    }

    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 归因回调状态
     * @param clazz 当前类
     * @param status
    const val LISTENER_STATUS_OPTION = 1//完成操作
    const val LISTENER_STATUS_WAIT = 2//等待对方确定情况
     **/
    @Synchronized
    override fun onAttributes(clazz: Class<*>, status: Int) {
        if (mOnIsReportServiceSuccessListener.isReportServiceSuccess()) {//如果有已经处理过了，就不要再处理了
            return
        }
        if (clazz == GoogleReferrerClient::class.java && status == AbsAttributes.LISTENER_STATUS_OPTION) {//当ReferrerClient 直接满足canFormatUrlFromGpCallBack 会触发该方法
            mergeAndReport(
                mGoogleReferrerClient.onGetInstallGoogleInfoBean(),
                mAppsFlyerReferrerClient.mInstallAppsflyerInfoBean
            )
        } else {//处理 ReferrerClient 与 AppsFlyer 的LISTENER_STATUS_WAIT
            mStatusMap[clazz] = status
            if (mStatusMap.size == 2) {//拿到了2次通知回调
                mergeAndReport(
                    mGoogleReferrerClient.onGetInstallGoogleInfoBean(),
                    mAppsFlyerReferrerClient.mInstallAppsflyerInfoBean
                )
            }
        }
    }


    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 合并 && 开始上报
     **/
    private fun mergeAndReport(
        googleInfoBean: InstallInfoBean?,
        appsFlyerInfoBean: InstallInfoBean?
    ) {
        var installInfoBean: InstallInfoBean? = null
        if (googleInfoBean != null && ReferrerUrlUtil.canFormatUrlFromGpCallBack(googleInfoBean.referrerUrl)) {
            installInfoBean = googleInfoBean
        } else if (appsFlyerInfoBean != null) {
            installInfoBean = appsFlyerInfoBean
        } else {
            installInfoBean = googleInfoBean
        }

        if (installInfoBean == null) {
            return
        }

        if (SaveInfoUtil.getInstallInfoBeanFromLocal() == null || SaveInfoUtil.getInstallInfoBeanFromLocal()?.infoSource.isNullOrEmpty()) {
            SaveInfoUtil.saveInstallInfoBeanToLocal(installInfoBean)
        }

        saveInstallInfoToYoadxLib(installInfoBean)

        reportPlayStoreInstall(mApplication, installInfoBean)
    }


    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 保存 installInfoBean 到本地
     **/
     fun saveInstallInfoToYoadxLib(installInfoBean: InstallInfoBean) {
        val forYoadxInstallInfoBean = ForYoadxInstallInfoBean()
        forYoadxInstallInfoBean.utmSource = installInfoBean.utmSource
        forYoadxInstallInfoBean.utmCampaign = installInfoBean.utmCampaign
        forYoadxInstallInfoBean.setmUtmCampaignNamem(installInfoBean.utmCampaignName)
        forYoadxInstallInfoBean.utmContent = installInfoBean.utmContent
        forYoadxInstallInfoBean.utmMedium = installInfoBean.utmMedium
        forYoadxInstallInfoBean.setmUtmMediumName(installInfoBean.utmMediumName)
        forYoadxInstallInfoBean.utmCountry = installInfoBean.utmCountry
        forYoadxInstallInfoBean.utmCreativeId = installInfoBean.utmCreativeId
        forYoadxInstallInfoBean.setmUtmCreativeName(installInfoBean.utmCreativeName)
        forYoadxInstallInfoBean.setmUtmMediumSource(installInfoBean.utmMediaSource)
        val gson = Gson().toJson(forYoadxInstallInfoBean)
        YoloCacheStorage.put(CacheConstants.KEY_USER_INSTALL_INFO_FOR_YOADX, gson, true)
    }


     fun reportPlayStoreInstall(
        context: Context,
        installInfoBean: InstallInfoBean
    ) {
        reportInstallToFirebase(context, installInfoBean)
        reportInstallToServer(installInfoBean)
    }


    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 上报到 fireBase
     **/
     fun reportInstallToFirebase(context: Context, cacheBean: InstallInfoBean) {
        if (!YoloCacheStorage.getBoolean(CacheConstants.KEY_HAS_REPORT_INSTALL_INFO, false)) {
            mAppReportUtils.initChannelProperty(context, cacheBean)
            mAppReportUtils.reportPlayStoreInstallToFirebase(context, cacheBean)
        }
    }


    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 告诉服务器
     **/
     fun reportInstallToServer(installInfoBean: InstallInfoBean) {
        if (!mOnIsReportServiceSuccessListener.isReportServiceSuccess()) {
            val request = InstallParamsRequest()
            request.mGpReferrer = installInfoBean.gpReferrerUrl
            request.mInfoSource = installInfoBean.infoSource
            request.mCpiCost = installInfoBean.cpiCost
            request.mAdjustReferrer = installInfoBean.adjustReferrer
            request.mAfReferrer = installInfoBean.afReferrer
            request.cnl = installInfoBean.cnl
            request.pCnl = installInfoBean.pCnl
            request.utmSource = installInfoBean.utmSource
            request.utmCampaign = installInfoBean.utmCampaign
            request.utmCampaignName = installInfoBean.utmCampaignName
            request.utmMedium = installInfoBean.utmMedium
            request.utmMediumName = installInfoBean.utmMediumName
            request.utmContent = installInfoBean.utmContent
            request.utmCountry = installInfoBean.utmCountry
            request.utmCreativeId = installInfoBean.utmCreativeId
            request.utmCreativeName = installInfoBean.utmCreativeName
            request.fbInstallReferrer = installInfoBean.adjustFbReferrer
            mYoloReport.reportFirstOpenToServer(request)
        }
    }


}