package com.yolo.referrerlibrary.attribute.google

import android.app.Application
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.yolo.base.installl.attributes.inter.OnGetInstallGoogleInfoBeanListener
import com.yolo.base.installl.attributes.util.ReferrerUrlUtil
import com.yolo.cache.storage.YoloCacheStorage
import com.yolo.referrerlibrary.base.AbsAttributes
import com.yolo.referrerlibrary.bean.InstallInfoBean
import com.yolo.referrerlibrary.constans.Config

/**
  * @date 创建时间: 2023/7/20
  * @auther gxx
  * @description 处理 InstallReferrerClient
  **/
class GoogleReferrerClient(val mApplication: Application): AbsAttributes(mApplication), InstallReferrerStateListener,
    OnGetInstallGoogleInfoBeanListener {
    companion object{
        const val SOURCE = "utm_source"
        const val PID = "pid"
        const val MEDIUM = "utm_medium"
        const val CNL = "cnl"
        const val CAMPAIGN = "utm_campaign"
        const val CONTENT = "utm_content"
        const val INFO_SOURCE = "info_source"
        const val CPI_COST = "cpi_cost"
        const val GAID = "gaid"
        const val GCLID = "gclid"
        const val AF_TRANID = "af_tranid"


        const val UTM_SOURCE_VALUE_GG_ADS = "Google Ads ACI"
        const val UTM_SOURCE_VALUE_AF_VALUE_NULL = "Af value null"

        const val ADJUST = "adjust"
        const val KOCHAVA = "kochava"
    }
    var mReferrerClient: InstallReferrerClient = InstallReferrerClient.newBuilder(mApplication).build()

    init {
        mReferrerClient.startConnection(this)
    }

    private val mReferralParams= HashMap<String,String>()

    private var mGpReferrerUrl = ""

    private var mInstallGoogleInfoBean: InstallInfoBean? = null



    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description google 回调
     **/
    override fun onInstallReferrerSetupFinished(responseCode: Int) {
        when (responseCode) {
            InstallReferrerClient.InstallReferrerResponse.OK -> {//成功回调
                kotlin.runCatching {
                    val referrerDetails = mReferrerClient.installReferrer
                    // String gpReferrerUrl = "utm_source=yoadx_adcash&utm_medium=1&utm_content=cid%3D16177909561077999533116284147652675&utm_campaign=255778020";
                    var gpReferrerUrl = "";
                    if (referrerDetails != null) {
                        gpReferrerUrl = referrerDetails.installReferrer + "&info_source=gp";
                        //gpReferrerUrl = "af_tranid=Auv4L61fjvnaGNmAvlPUtQ&pid=adsterra_int&c=756863&af_siteid=17044513&af_sub_siteid=1798924&af_click_lookback=7d&clickid=2435321dd815bb7f3694e845faec1704" + "&info_source=gp";
                        mGpReferrerUrl = "$gpReferrerUrl&info_source=gp"
                        //mGpReferrerUrl = "af_tranid=Auv4L61fjvnaGNmAvlPUtQ&pid=adsterra_int&c=756863&af_siteid=17044513&af_sub_siteid=1798924&af_click_lookback=7d&clickid=2435321dd815bb7f3694e845faec1704";
                    }
                    mInstallGoogleInfoBean = optionInstallGoogleReferrer(gpReferrerUrl)
                    if (mInstallGoogleInfoBean!=null){
                        mInstallGoogleInfoBean!!.gpReferrerUrl= mGpReferrerUrl
                        mInstallGoogleInfoBean!!.infoSource = "gp"
                        //存储到share里面
                        saveGpReferrerUtmSource(mInstallGoogleInfoBean!!.utmSource)
                        //如果包含特殊类型的，直接完成操作
                        if (ReferrerUrlUtil.canFormatUrlFromGpCallBack(gpReferrerUrl)){
                            mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_OPTION)
                        }else{
                            mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
                        }
                    }else{
                        mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
                    }
                }.onFailure {
                    it.printStackTrace()
                    mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
                }

            }

            InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED -> {
                mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
            }

            InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE -> {
                mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
            }
        }
    }

    /**
      * @date 创建时间: 2023/7/20
      * @auther gxx
      * @description 断开链接
      **/
    override fun onInstallReferrerServiceDisconnected() {
        mOnAttributesListener?.onAttributes(this::class.java,LISTENER_STATUS_WAIT)
    }

    /**
      * @date 创建时间: 2023/7/20
      * @auther gxx
      * @description 存储 gpReferrerUtmSource
      **/
    private fun saveGpReferrerUtmSource(gpReferrerUtmSource:String){
        if (gpReferrerUtmSource.isEmpty()){
            return
        }
        YoloCacheStorage.put<String>(
            Config.KEY_USER_GP_REFERRER_UTM_SOURCE,gpReferrerUtmSource
        )
    }

    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 拆分googl 提供的参数，转换成bean
     * @param referrerUrl google 提供的参数拼接
     **/
    private fun optionInstallGoogleReferrer(referrerUrl:String ): InstallInfoBean? {
        if (referrerUrl.isEmpty()){
            return null
        }
        val installInfoBean = InstallInfoBean()
        val params = referrerUrl.split("$")
        for (param in params) {
            if (param.isNullOrEmpty()){
                continue
            }
            val pairs = param.split("=")
            if (pairs.size == 2){
                val key = pairs[0]
                val value = pairs[1]
                if (!key.isNullOrEmpty() && !value.isNullOrEmpty()){
                    mReferralParams[key] = value
                }
            }

            installInfoBean.apply {
                this.cnl = mReferralParams[CNL]?:""
                this.utmSource = mReferralParams[SOURCE]
                if (this.utmSource.isNullOrEmpty()){
                    this.utmSource = mReferralParams[PID]
                }
                if (this.utmSource.isNullOrEmpty()){
                    if (mReferralParams.contains(GCLID)){
                        this.utmSource = UTM_SOURCE_VALUE_GG_ADS
                    }else if (mReferralParams.contains(AF_TRANID)){
                        this.utmSource = UTM_SOURCE_VALUE_AF_VALUE_NULL
                    }else{
                        this.utmSource = ""
                    }
                }
                this.utmCampaign = mReferralParams[CAMPAIGN]?:""
                this.utmMedium = mReferralParams[MEDIUM]?:""
                this.utmContent = mReferralParams[CONTENT]?:""
                this.infoSource = mReferralParams[INFO_SOURCE]?:""
                this.cpiCost = mReferralParams[CPI_COST]?:""
                this.referrerUrl = referrerUrl
            }
        }


        return installInfoBean
    }

    /**
      * @date 创建时间: 2023/7/20
      * @auther gxx
      * @description 返回 google提供的bean
      **/
    override fun onGetInstallGoogleInfoBean(): InstallInfoBean? {
        return mInstallGoogleInfoBean
    }


}