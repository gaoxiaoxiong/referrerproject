package com.yolo.referrerlibrary.attribute.appsflyer.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.yolo.referrerlibrary.bean.InstallInfoBean;
import com.yolo.referrerlibrary.inter.OnAttributionAppsflyerCreatorListener;

import java.util.Map;

/**
 * @author zhaibinme on 2022/3/10
 */
public class FacebookAppsflyerAttributionAdapter implements OnAttributionAppsflyerCreatorListener {

    public  boolean isFacebookChannel(Map<String, Object> conversionDataMap) {
        if (conversionDataMap == null) {
            return false;
        }

        Object network = conversionDataMap.get("media_source");
        if (network == null || TextUtils.isEmpty(network.toString())) {
            return false;
        }

        String utm_source = network.toString().toLowerCase();

        if (utm_source.contains("facebook") || utm_source.contains("fb") || utm_source.contains("face") || utm_source.contains("instagram") || utm_source.contains("audience") || utm_source.contains("messenger")) {
            return true;
        }

        return false;
    }

    @NonNull
    @Override
    public InstallInfoBean attributionAppsflyerCreator(@NonNull Map<String, Object> conversionDataMap) {
        try {
            InstallInfoBean installInfoBean;
            String utm_source = "Facebook";

            Object utm_campaign = conversionDataMap.get("campaign_id");
            Object utm_campaign_name = conversionDataMap.get("campaign");

            Object utm_medium = conversionDataMap.get("adset_id");
            Object utm_medium_name = conversionDataMap.get("adset");

            Object utm_creative_id = conversionDataMap.get("adgroup_id");
            Object utm_creative_name = conversionDataMap.get("adgroup");

            String referrerUrl = "attribution=" + conversionDataMap.toString();

            installInfoBean = new InstallInfoBean();
            installInfoBean.setCnl(utm_source);
            installInfoBean.setUtmSource(utm_source);
            installInfoBean.setUtmCampaign(utm_campaign == null ? "" : utm_campaign.toString());
            installInfoBean.setUtmCampaignName(utm_campaign_name == null ? "" : utm_campaign_name.toString());
            installInfoBean.setUtmMedium(utm_medium == null ? "" : utm_medium.toString());
            installInfoBean.setUtmMediumName(utm_medium_name == null ? "" : utm_medium_name.toString());
            installInfoBean.setInfoSource("appsflyer");
            installInfoBean.setReferrerUrl(referrerUrl);
            installInfoBean.setAfReferrer(referrerUrl);
            installInfoBean.setUtmCreativeId(utm_creative_id == null ? "" : utm_creative_id.toString());
            installInfoBean.setUtmCreativeName(utm_creative_name == null ? "" : utm_creative_name.toString());
            return installInfoBean;
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return new InstallInfoBean();
    }
}
