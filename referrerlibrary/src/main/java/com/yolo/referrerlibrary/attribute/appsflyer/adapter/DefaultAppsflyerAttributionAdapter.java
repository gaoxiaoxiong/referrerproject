package com.yolo.referrerlibrary.attribute.appsflyer.adapter;

import androidx.annotation.NonNull;

import com.yolo.referrerlibrary.bean.InstallInfoBean;
import com.yolo.referrerlibrary.inter.OnAttributionAppsflyerCreatorListener;

import java.util.Map;

/**
 * @author zhaibinme on 2022/3/7
 */
public class DefaultAppsflyerAttributionAdapter implements OnAttributionAppsflyerCreatorListener {



    @NonNull
    @Override
    public InstallInfoBean attributionAppsflyerCreator(@NonNull Map<String, Object> conversionDataMap) {
        try {
            InstallInfoBean installInfoBean;
            Object utm_source = conversionDataMap.get("media_source");
            Object utm_campaign = conversionDataMap.get("campaign_id");
            Object utm_campaign_name = conversionDataMap.get("campaign");

            Object utm_medium = conversionDataMap.get("af_siteid");

            Object utm_creative_id = conversionDataMap.get("af_ad_id");
            Object utm_creative_name = conversionDataMap.get("af_ad");

            String referrerUrl = "attribution=" + conversionDataMap.toString();

            installInfoBean = new InstallInfoBean();
            installInfoBean.setUtmSource(utm_source == null ? "" : utm_source.toString());
            installInfoBean.setUtmCampaign(utm_campaign == null ? "" : utm_campaign.toString());
            installInfoBean.setUtmCampaignName(utm_campaign_name == null ? "" : utm_campaign_name.toString());
            installInfoBean.setUtmMedium(utm_medium == null ? "" : utm_medium.toString());
            installInfoBean.setReferrerUrl(referrerUrl);
            installInfoBean.setAfReferrer(referrerUrl);
            installInfoBean.setInfoSource("appsflyer");
            installInfoBean.setCnl(utm_source == null ? "" : utm_source.toString());
            installInfoBean.setUtmCreativeId(utm_creative_id == null ? "" : utm_creative_id.toString());
            installInfoBean.setUtmCreativeName(utm_creative_name == null ? "" : utm_creative_name.toString());
            return installInfoBean;
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return new InstallInfoBean();
    }
}
