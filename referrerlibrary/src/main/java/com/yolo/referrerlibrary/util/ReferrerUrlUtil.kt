package com.yolo.base.installl.attributes.util

import com.yolo.referrerlibrary.attribute.google.GoogleReferrerClient

/**
  * @date 创建时间: 2023/7/20
  * @auther gxx
  * @description 推荐人工具类
  **/
class ReferrerUrlUtil {
    companion object{
        const val VID_MATE = "vidmate"
        const val YO_ADX = "yoadx"
        const val VID = "vid"
        const val VDM = "vdm"
        
        /**
         * @date 创建时间: 2023/7/20
         * @auther gxx
         * @description 判断当前url是否包含特殊的字段信息
         **/
        fun canFormatUrlFromGpCallBack(currentReferrerUrl:String):Boolean{
            if (currentReferrerUrl.isEmpty()){
                return false
            }
            if (currentReferrerUrl.contains(VID_MATE) || currentReferrerUrl.contains(
                    YO_ADX
                ) || currentReferrerUrl.contains(
                    VID
                ) || currentReferrerUrl.contains(VDM)){
                return true
            }

            if (currentReferrerUrl.contains(GoogleReferrerClient.ADJUST) || currentReferrerUrl.contains(
                    GoogleReferrerClient.KOCHAVA
                )){
                return false
            }

            return false

        }
    }
}