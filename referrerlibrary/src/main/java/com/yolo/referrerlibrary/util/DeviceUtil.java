package com.yolo.referrerlibrary.util;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by augustdong on 17/6/12.
 */

public class DeviceUtil {


    /**
     * 获取mcc
     * @param context
     * @return
     */
    public static String getMcc(@NonNull Context context) {
        if (context == null) {
            return "";
        }

        String mcc_mnc = getSimOperator(context);

        if (mcc_mnc != null && mcc_mnc.length() >= 3) {
            StringBuilder mcc = new StringBuilder();
            mcc.append(mcc_mnc, 0, 3);

            return mcc.toString();
        }

        return "";
    }


    /**
     * 获取唯一ID
     * @param context
     * @return
     */
    public static String getUniqueID(@NonNull Context context) {
//        if (true) {
//            return "ewqdsdsa3342dasdsad";
//        }

        if (context == null) {
            return "";
        }

        String uniqueID = getAndroidID(context);

        if (!TextUtils.isEmpty(uniqueID)) {
            return uniqueID;
        }

        uniqueID = getSerialID();

        if (!TextUtils.isEmpty(uniqueID)) {
            return "s" + uniqueID;
        }

        return "";
    }

    private static String getSerialID() {
        try {
            String serialID = Build.SERIAL;

            if (TextUtils.isEmpty(serialID)) {
                return "";
            }

            return serialID;
        } catch (Exception e) {
            e.printStackTrace();

            return "";
        }
    }


    /**
     * 获取mnc
     * @param context
     * @return
     */
    public static String getMnc(@NonNull Context context) {
        if (context == null) {
            return "";
        }

        String mcc_mnc = getSimOperator(context);

        if (mcc_mnc != null && mcc_mnc.length() >= 5) {
            StringBuilder mnc = new StringBuilder();
            mnc.append(mcc_mnc, 3, 5);

            return mnc.toString();
        }

        return "";
    }



    /**
     * 获取系统国家_语言
     */
    @NonNull
    public static String getOSRegionCodeLang(@NonNull Context context) {
        Locale locale = getLocale(context);
        if (locale == null) {
            return "";
        }

        return locale.getCountry() + "_" + locale.getLanguage();
    }

    private static String getAndroidID(@NonNull Context context) {
        try {
            ContentResolver cr = context.getContentResolver();
            String androidID = Settings.Secure.getString(cr, Settings.Secure.ANDROID_ID);

            if (TextUtils.isEmpty(androidID)) {
                return "";
            }

            if (checkNumberAsHexStringWith1to32(androidID)) {
                return androidID;
            }

            return "";
        } catch (Exception e) {
            e.printStackTrace();

            return "";
        }
    }



    private static boolean checkNumberAsHexStringWith1to32(String number) {
        if (TextUtils.isEmpty(number)) {
            return false;
        }

        String reg = "[a-f0-9A-F]{1,32}";
        if (Pattern.matches(reg, number)) {
            return true;
        }

        return false;
    }

    private static String getSimOperator(Context context) {
        if (context == null) {
            return null;
        }

        TelephonyManager tm = getTelephonyManager(context);
        if (tm == null) {
            return null;
        }

        return tm.getSimOperator();
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        if (context == null) {
            return null;
        }

        try {
            return (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Nullable
    public static Locale getLocale(@NonNull Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        }

        return context.getResources().getConfiguration().locale;
    }


}
