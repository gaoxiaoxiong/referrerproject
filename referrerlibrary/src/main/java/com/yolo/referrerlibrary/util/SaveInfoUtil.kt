package com.yolo.base.installl.attributes.util

import com.yolo.cache.storage.CacheConstants.KEY_USER_INSTALL_INFO
import com.yolo.cache.storage.YoloCacheStorage
import com.yolo.referrerlibrary.bean.InstallInfoBean

class SaveInfoUtil {
    companion object{
        /**
         * @date 创建时间: 2023/7/20
         * @auther gxx
         * @description 存储InstallInfoBean到本地
         **/
         fun saveInstallInfoBeanToLocal(infoBean: InstallInfoBean) {
            YoloCacheStorage.put<InstallInfoBean>(KEY_USER_INSTALL_INFO, infoBean, true)
        }


        /**
         * @date 创建时间: 2023/7/20
         * @auther gxx
         * @description 获取本地存储的 InstallInfoBean
         **/
         fun getInstallInfoBeanFromLocal(): InstallInfoBean? {
            return YoloCacheStorage.getData<InstallInfoBean>(
                KEY_USER_INSTALL_INFO, null,
                InstallInfoBean::class.java
            )
        }

    }
}