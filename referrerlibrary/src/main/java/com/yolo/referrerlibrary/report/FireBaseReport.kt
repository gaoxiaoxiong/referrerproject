package com.yolo.referrerlibrary.report

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import com.google.firebase.analytics.FirebaseAnalytics
import com.yolo.cache.storage.CacheConstants
import com.yolo.cache.storage.YoloCacheStorage
import com.yolo.referrerlibrary.bean.InstallInfoBean
import com.yolo.referrerlibrary.constans.Config

class FireBaseReport {
    private val TAG = "FireBaseReport"

    object Param {
        const val UD_UTM_SOURCE = "ud_utm_source"
        const val UD_CNL = "ud_cnl"
        const val UD_UTM_MEDIUM = "ud_utm_medium"
        const val UD_UTM_CAMPAIGN = "ud_utm_campaign"
        const val UD_UTM_CONTENT = "ud_utm_content"
        const val UD_UTM_INFO_SOURCE = "ud_utm_info_source"
        const val UD_UTM_CREATIVE_ID = "ud_utm_creative_id"
        const val UD_UTM_COUNTRY = "ud_utm_country"
    }

    fun initChannelProperty(context: Context?, bean: InstallInfoBean?) {
        val firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
        if (bean != null) {
            firebaseAnalytics.setUserProperty("utm_source", length35SubString(bean.utmSource))
            firebaseAnalytics.setUserProperty("utm_campaign", length35SubString(bean.utmCampaign))
            firebaseAnalytics.setUserProperty(
                "utm_campaign_name",
                length35SubString(bean.utmCampaignName)
            )
            firebaseAnalytics.setUserProperty("utm_content", length35SubString(bean.utmContent))
            firebaseAnalytics.setUserProperty("utm_medium", length35SubString(bean.utmMedium))
            firebaseAnalytics.setUserProperty(
                "utm_medium_name",
                length35SubString(bean.utmMediumName)
            )
            firebaseAnalytics.setUserProperty(
                "utm_creative_id",
                length35SubString(bean.utmCreativeId)
            )
            firebaseAnalytics.setUserProperty(
                "utm_creative_name",
                length35SubString(bean.utmCreativeName)
            )
        }
    }

    fun reportPlayStoreInstallToFirebase(context: Context, installInfoBean: InstallInfoBean) {
        YoloCacheStorage.put(CacheConstants.KEY_HAS_REPORT_INSTALL_INFO, true)
        val bundle = Bundle()
        val source = length100SubString(installInfoBean.utmSource)
        val medium = length100SubString(installInfoBean.utmMedium)
        val campaign = length100SubString(installInfoBean.utmCampaign)
        bundle.putString(Param.UD_UTM_SOURCE, source)
        bundle.putString(Param.UD_CNL, length100SubString(installInfoBean.cnl))
        bundle.putString(Param.UD_UTM_MEDIUM, medium)
        bundle.putString(Param.UD_UTM_CAMPAIGN, campaign)
        bundle.putString(Param.UD_UTM_CONTENT, length100SubString(installInfoBean.utmContent))
        bundle.putString(Param.UD_UTM_INFO_SOURCE, length100SubString(installInfoBean.infoSource))
        bundle.putString(Param.UD_UTM_CREATIVE_ID, length100SubString(installInfoBean.utmCreativeId))
        bundle.putString(Param.UD_UTM_COUNTRY, length100SubString(installInfoBean.utmCountry))
        reportFirebaseParams(context, source, medium, campaign)
    }

    private fun length35SubString(value: String): String {
        if (TextUtils.isEmpty(value)) {
            return value
        }
        return if (value.length > 35) {
            value.substring(0, 34)
        } else value
    }

    /**
      * @date 创建时间: 2023/7/21
      * @auther gxx
      * @description 长度超过100进行截取
      **/
    private fun length100SubString(value: String): String {
        if (TextUtils.isEmpty(value)) {
            return value
        }
        return if (value.length > 100) {
            value.substring(0, 98)
        } else value
    }

    /**
      * @date 创建时间: 2023/7/21
      * @auther gxx
      * @description 上传到fireBase
      **/
   private fun reportFirebaseParams(
        context: Context?,
        source: String?,
        medium: String?,
        campaign: String?
    ) {
        val firebaseAnalytics = FirebaseAnalytics.getInstance(context!!)
        val params = Bundle()
        params.putString(FirebaseAnalytics.Param.CAMPAIGN, campaign)
        params.putString(FirebaseAnalytics.Param.MEDIUM, medium)
        params.putString(FirebaseAnalytics.Param.SOURCE, source)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.CAMPAIGN_DETAILS, params)
    }


}