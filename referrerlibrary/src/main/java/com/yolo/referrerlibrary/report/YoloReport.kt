package com.yolo.referrerlibrary.report


import com.yolo.cache.storage.YoloCacheStorage
import com.yolo.referrerlibrary.bean.InstallInfoBean
import com.yolo.referrerlibrary.bean.InstallParamsRequest
import com.yolo.referrerlibrary.constans.Config
import com.yolo.referrerlibrary.network.RequestPresenter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.launch

/**
 * @date 创建时间: 2023/7/21
 * @auther gxx
 * @description 上传到yoloService
 **/
class YoloReport {
    private val TAG = "YoloReport"
    private val mCoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.Default)
    private val mRequestPresenter = RequestPresenter()//网络请求
    private var isInstallReport = false

    /**
     * @date 创建时间: 2023/7/21
     * @auther gxx
     * @description 上报到服务器
     **/
    fun reportFirstOpenToServer(requestBean: InstallParamsRequest) {
        if (isInstallReport || YoloCacheStorage.getBoolean(
                Config.KEY_REPORT_INSTALL_CALL_SERVER_SUCCESSFUL,
                false
            )
        ) {
            return
        }
        isInstallReport = true

        mCoroutineScope.launch {
            kotlin.runCatching {
                mRequestPresenter.readFirstOpen(requestBean).onCompletion {
                    isInstallReport = false
                }
                    .collect {
                        YoloCacheStorage.put(Config.KEY_REPORT_INSTALL_CALL_SERVER_SUCCESSFUL, true);
                        val installInfoBean = YoloCacheStorage.getData(
                            Config.KEY_USER_INSTALL_INFO,
                            null,
                            InstallInfoBean::class.java
                        ) as InstallInfoBean?
                        if (installInfoBean != null && !installInfoBean.cnl.isNullOrEmpty()) {
                            installInfoBean.cnl = it.cnl
                            YoloCacheStorage.put(Config.KEY_USER_INSTALL_INFO, installInfoBean, true)
                        }
                    }
            }.onFailure {
                it.printStackTrace()
                isInstallReport = false
            }

        }
    }
}