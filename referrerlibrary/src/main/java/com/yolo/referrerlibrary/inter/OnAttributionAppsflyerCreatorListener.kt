package com.yolo.referrerlibrary.inter

import com.yolo.referrerlibrary.bean.InstallInfoBean

interface OnAttributionAppsflyerCreatorListener {
    fun attributionAppsflyerCreator(conversionDataMap:MutableMap<String,Any>): InstallInfoBean
}