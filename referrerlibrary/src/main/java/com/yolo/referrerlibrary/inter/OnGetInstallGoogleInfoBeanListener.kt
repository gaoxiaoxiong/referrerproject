package com.yolo.base.installl.attributes.inter

import com.yolo.referrerlibrary.bean.InstallInfoBean

/**
  * @date 创建时间: 2023/7/20
  * @auther gxx
  * @description 获取google Client 里面的 InstallGoogleInfoBean
  **/
interface OnGetInstallGoogleInfoBeanListener {
    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 返回 google提供的bean
     **/
    fun onGetInstallGoogleInfoBean(): InstallInfoBean?
}