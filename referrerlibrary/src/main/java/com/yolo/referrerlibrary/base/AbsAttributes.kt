package com.yolo.referrerlibrary.base

import android.app.Application

abstract class AbsAttributes(private val mApplication: Application) {

    companion object {
        const val LISTENER_STATUS_DEFAULT = 0//默认值
        const val LISTENER_STATUS_OPTION = 1//完成操作
        const val LISTENER_STATUS_WAIT = 2//等待对方确定情况
    }

    protected var mOnAttributesListener: OnAttributesListener? = null

    fun setOnAttributesListener(listener: OnAttributesListener) {
        this.mOnAttributesListener = listener
    }

    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 归因初始化成功失败的回调
     **/
    interface OnAttributesListener {
        /**
         * @date 创建时间: 2023/7/20
         * @auther gxx
         * @description 归因回调状态
         * @param clazz 当前类
         * @param status 详见 AbsAttributes.LISTENER_STATUS_*
         **/
        fun onAttributes(clazz: Class<*>, status: Int)
    }


}