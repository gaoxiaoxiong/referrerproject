package com.yolo.referrerlibrary.bean;


import com.yolo.referrerlibrary.constans.Config;

import java.io.Serializable;

/**
 * @author zhaibinme on 2020/8/13
 */
public class InstallInfoBean implements Serializable {

    private String mCnl;

    private String mPCnl;

    private String mUtmSource;

    private String mGpReferrerUtmSource;

    private String mUtmCampaign;

    private String mUtmCampaignName;

    private String mUtmMediaSource;

    private String mUtmContent;

    private String mUtmMedium;

    private String mUtmMediumName;

    private String mReferrerUrl;

    private String mInfoSource;

    private String mCpiCost;

    private String mUtmCreativeId;

    private String mUtmCreativeName;

    private String mUtmCountry;

    private String mGpReferrerUrl = "";

    private String mAdjustReferrer = "";

    private String mAdjustFbReferrer = "";

    private String mAfReferrer = "";

    public String getUtmMediaSource() {
        return mUtmMediaSource;
    }

    public void setUtmMediaSource(String mUtmMediaSource) {
        this.mUtmMediaSource = mUtmMediaSource;
    }

    public void setAdjustReferrer(String adjustReferrer) {
        mAdjustReferrer = adjustReferrer;
    }

    public String getAdjustReferrer() {
        return mAdjustReferrer;
    }

    public String getAfReferrer() {
        return mAfReferrer;
    }

    public void setAfReferrer(String afReferrer) {
        mAfReferrer = afReferrer;
    }

    public void setGpReferrerUrl(String referrerUrl) {
        mGpReferrerUrl = referrerUrl;
    }

    public String getGpReferrerUrl() {
        return mGpReferrerUrl;
    }

    public InstallInfoBean() {
        mPCnl = Config.Companion.getChannel();
        mCnl = mPCnl;
    }

    public String getCnl() {
        return mCnl;
    }

    public void setCnl(String cnl) {
        mCnl = cnl;
    }

    public String getPCnl() {
        return mPCnl;
    }

    public String getUtmSource() {
        return mUtmSource;
    }

    public void setUtmSource(String utmSource) {
        mUtmSource = utmSource;
    }

    public String getUtmCampaign() {
        return mUtmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        mUtmCampaign = utmCampaign;
    }

    public String getUtmCampaignName() {
        return mUtmCampaignName;
    }

    public void setUtmCampaignName(String utmCampaignName) {
        mUtmCampaignName = utmCampaignName;
    }

    public String getUtmContent() {
        return mUtmContent;
    }

    public void setUtmContent(String utmContent) {
        mUtmContent = utmContent;
    }

    public String getUtmMedium() {
        return mUtmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        mUtmMedium = utmMedium;
    }

    public String getUtmMediumName() {
        return mUtmMediumName;
    }

    public void setUtmMediumName(String utmMediumName) {
        mUtmMediumName = utmMediumName;
    }

    public String getReferrerUrl() {
        return mReferrerUrl;
    }

    public void setReferrerUrl(String referrerUrl) {
        mReferrerUrl = referrerUrl;
    }

    public String getAdjustFbReferrer() {
        return mAdjustFbReferrer;
    }

    public void setAdjustFbReferrer(String adjustFbReferrer) {
        mAdjustFbReferrer = adjustFbReferrer;
    }

    public String getInfoSource() {
        return mInfoSource;
    }

    public void setInfoSource(String infoSource) {
        mInfoSource = infoSource;
    }

    public String getCpiCost() {
        return mCpiCost;
    }

    public void setCpiCost(String cpiCost) {
        mCpiCost = cpiCost;
    }

    public String getUtmCreativeId() {
        return mUtmCreativeId;
    }

    public void setUtmCreativeId(String utmCreativeId) {
        mUtmCreativeId = utmCreativeId;
    }

    public String getUtmCreativeName() {
        return mUtmCreativeName;
    }

    public void setUtmCreativeName(String utmCreativeName) {
        mUtmCreativeName = utmCreativeName;
    }

    public String getUtmCountry() {
        return mUtmCountry;
    }

    public void setUtmCountry(String utmCountry) {
        mUtmCountry = utmCountry;
    }

    public String getGpReferrerUtmSource() {
        return mGpReferrerUtmSource;
    }

    public void setGpReferrerUtmSource(String gpReferrerUtmSource) {
        mGpReferrerUtmSource = gpReferrerUtmSource;
    }
}
