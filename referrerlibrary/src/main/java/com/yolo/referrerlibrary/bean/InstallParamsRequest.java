package com.yolo.referrerlibrary.bean;

import com.google.gson.annotations.SerializedName;


/**
 * @author zhaibinme on 2020/5/29
 */
public class InstallParamsRequest   {

    @SerializedName("gp_referrer")
    public String mGpReferrer;

    @SerializedName("ad_referrer")
    public String mAdjustReferrer;

    @SerializedName("af_referrer")
    public String mAfReferrer;

    @SerializedName("info_source")
    public String mInfoSource;

    @SerializedName("cpi_cost")
    public String mCpiCost;

    @SerializedName("cnl")
    private String mCnl = "";

    @SerializedName("pcnl")
    private String mPCnl = "";

    @SerializedName("utm_source")
    private String mUtmSource = "";

    @SerializedName("utm_campaign")
    private String mUtmCampaign = "";

    @SerializedName("utm_campaign_name")
    private String mUtmCampaignName = "";

    @SerializedName("utm_medium")
    private String mUtmMedium = "";

    @SerializedName("utm_medium_name")
    private String mUtmMediumName = "";

    @SerializedName("utm_content")
    private String mUtmContent = "";

    @SerializedName("utm_country")
    private String mUtmCountry = "";

    @SerializedName("utm_creative_id")
    private String mUtmCreativeId = "";

    @SerializedName("utm_creative_name")
    private String mUtmCreativeName = "";

    @SerializedName("fb_install_referrer")
    private String fbInstallReferrer;


    public String getFbInstallReferrer() {
        return fbInstallReferrer;
    }

    public void setFbInstallReferrer(String fbInstallReferrer) {
        this.fbInstallReferrer = fbInstallReferrer;
    }

    public String getUtmCreativeName() {
        return mUtmCreativeName;
    }

    public void setUtmCreativeName(String utmCreativeName) {
        mUtmCreativeName = utmCreativeName;
    }

    public String getUtmCreativeId() {
        return mUtmCreativeId;
    }

    public void setUtmCreativeId(String utmCreativeId) {
        mUtmCreativeId = utmCreativeId;
    }


    public String getUtmCountry() {
        return mUtmCountry;
    }

    public void setUtmCountry(String utmCountry) {
        mUtmCountry = utmCountry;
    }


    public String getUtmContent() {
        return mUtmContent;
    }

    public void setUtmContent(String utmContent) {
        mUtmContent = utmContent;
    }

    public String getUtmMedium() {
        return mUtmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        mUtmMedium = utmMedium;
    }

    public String getUtmMediumName() {
        return mUtmMediumName;
    }

    public void setUtmMediumName(String utmMediumName) {
        mUtmMediumName = utmMediumName;
    }

    public String getUtmCampaignName() {
        return mUtmCampaignName;
    }

    public void setUtmCampaignName(String utmCampaignName) {
        mUtmCampaignName = utmCampaignName;
    }

    public String getUtmCampaign() {
        return mUtmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        mUtmCampaign = utmCampaign;
    }

    public String getCnl() {
        return mCnl;
    }

    public void setCnl(String cnl) {
        mCnl = cnl;
    }

    public String getPCnl() {
        return mPCnl;
    }

    public void setPCnl(String PCnl) {
        mPCnl = PCnl;
    }


    public String getUtmSource() {
        return mUtmSource;
    }

    public void setUtmSource(String utmSource) {
        mUtmSource = utmSource;
    }

}
