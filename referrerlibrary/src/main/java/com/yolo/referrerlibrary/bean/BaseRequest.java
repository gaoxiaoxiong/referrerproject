package com.yolo.referrerlibrary.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseRequest implements Serializable {


    @SerializedName("did")
    private String mDid = "";

    @SerializedName("cnl")
    private String mCnl = "";

    @SerializedName("gaid")
    private String mGaid = "";

    @SerializedName("pcnl")
    private String mPCnl = "";

    @SerializedName("utm_source")
    private String mUtmSource = "";

    @SerializedName("utm_content")
    private String mUtmContent = "";

    @SerializedName("utm_medium")
    private String mUtmMedium = "";

    @SerializedName("utm_medium_name")
    private String mUtmMediumName = "";

    @SerializedName("utm_campaign")
    private String mUtmCampaign = "";

    @SerializedName("utm_campaign_name")
    private String mUtmCampaignName = "";

    @SerializedName("utm_creative_id")
    private String mUtmCreativeId = "";

    @SerializedName("utm_creative_name")
    private String mUtmCreativeName = "";

    @SerializedName("utm_country")
    private String mUtmCountry = "";

    @SerializedName("mcc")
    private String mMcc = "";

    @SerializedName("mnc")
    private String mMnc = "";

    @SerializedName("pkg")
    private String mPkg = "";

    @SerializedName("lang")
    private String mLang = "";

    @SerializedName("cv")
    private String mCv = "";

    @SerializedName("ts")
    private long mTs;

    @SerializedName("uid")
    private String mUid = "";

    @SerializedName("token")
    private String mToken = "";

    @SerializedName("is_rooted")
    private boolean mIsRooted = false;

    @SerializedName("is_vpn_used")
    private boolean mIsVPNUsed = false;

    @SerializedName("phone_brand")
    private String phoneBrand = "";

    @SerializedName("phone_model")
    private String phoneModel = "";

    @SerializedName("os_ver")
    private String osVer = "";

    @SerializedName("os")
    private final String os = "Android";

    @SerializedName("fb_install_referrer")
    private String fbInstallReferrer;

    public String getDid() {
        return mDid;
    }

    public void setDid(String did) {
        mDid = did;
    }

    public String getCnl() {
        return mCnl;
    }

    public void setCnl(String cnl) {
        mCnl = cnl;
    }

    public String getGaid() {
        return mGaid;
    }

    public void setGaid(String gaid) {
        mGaid = gaid;
    }

    public String getPCnl() {
        return mPCnl;
    }

    public void setPCnl(String PCnl) {
        mPCnl = PCnl;
    }

    public String getUtmSource() {
        return mUtmSource;
    }

    public void setUtmSource(String utmSource) {
        mUtmSource = utmSource;
    }

    public String getUtmContent() {
        return mUtmContent;
    }

    public void setUtmContent(String utmContent) {
        mUtmContent = utmContent;
    }

    public String getUtmMedium() {
        return mUtmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        mUtmMedium = utmMedium;
    }

    public String getUtmMediumName() {
        return mUtmMediumName;
    }

    public void setUtmMediumName(String utmMediumName) {
        mUtmMediumName = utmMediumName;
    }

    public String getUtmCampaign() {
        return mUtmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        mUtmCampaign = utmCampaign;
    }

    public String getMcc() {
        return mMcc;
    }

    public void setMcc(String mcc) {
        mMcc = mcc;
    }

    public String getMnc() {
        return mMnc;
    }

    public void setMnc(String mnc) {
        mMnc = mnc;
    }

    public String getPkg() {
        return mPkg;
    }

    public void setPkg(String pkg) {
        mPkg = pkg;
    }

    public String getLang() {
        return mLang;
    }

    public void setLang(String lang) {
        mLang = lang;
    }

    public String getCv() {
        return mCv;
    }

    public void setCv(String cv) {
        mCv = cv;
    }

    public long getTs() {
        return mTs;
    }

    public void setTs(long ts) {
        mTs = ts;
    }

    public String getUid() {
        return mUid;
    }

    public void setUid(String uid) {
        mUid = uid;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public boolean isRooted() {
        return mIsRooted;
    }

    public void setIsRooted(boolean isRooted) {
        mIsRooted = isRooted;
    }

    public boolean isVPNUsed() {
        return mIsVPNUsed;
    }

    public void setIsVPNUsed(boolean isVPNUsed) {
        mIsVPNUsed = isVPNUsed;
    }

    public String getUtmCreativeId() {
        return mUtmCreativeId;
    }

    public void setUtmCreativeId(String utmCreativeId) {
        mUtmCreativeId = utmCreativeId;
    }

    public String getUtmCountry() {
        return mUtmCountry;
    }

    public void setUtmCountry(String utmCountry) {
        mUtmCountry = utmCountry;
    }

    public void setRooted(boolean rooted) {
        mIsRooted = rooted;
    }

    public void setVPNUsed(boolean VPNUsed) {
        mIsVPNUsed = VPNUsed;
    }

    public String getPhoneBrand() {
        return phoneBrand;
    }

    public void setPhoneBrand(String phoneBrand) {
        this.phoneBrand = phoneBrand;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getFbInstallReferrer() {
        return fbInstallReferrer;
    }

    public void setFbInstallReferrer(String fbInstallReferrer) {
        this.fbInstallReferrer = fbInstallReferrer;
    }

    public String getOsVer() {
        return osVer;
    }

    public void setOsVer(String osVer) {
        this.osVer = osVer;
    }

    public String getUtmCampaignName() {
        return mUtmCampaignName;
    }

    public void setUtmCampaignName(String utmCampaignName) {
        mUtmCampaignName = utmCampaignName;
    }

    public String getUtmCreativeName() {
        return mUtmCreativeName;
    }

    public void setUtmCreativeName(String utmCreativeName) {
        mUtmCreativeName = utmCreativeName;
    }
}
