package com.yolo.referrerlibrary.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author zhaibinme on 2020/8/13
 */
public class InstallCallResponse implements Serializable {

    @SerializedName("cnl")
    private String mCnl;

    public String getCnl() {
        return mCnl;
    }

    public void setCnl(String cnl) {
        mCnl = cnl;
    }
}
