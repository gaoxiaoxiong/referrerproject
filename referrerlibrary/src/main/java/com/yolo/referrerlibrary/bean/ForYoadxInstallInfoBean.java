package com.yolo.referrerlibrary.bean;


import com.yolo.referrerlibrary.constans.Config;

import java.io.Serializable;

/**
 * @author zhaibinme on 2021/5/26
 */
public class ForYoadxInstallInfoBean implements Serializable {
    private String mCnl;

    private String mUtmSource;

    private String mUtmCampaign;

    private String mUtmCampaignNamem;

    private String mUtmContent;

    private String mUtmMedium;

    private String mUtmMediumName;

    private String mUtmMediumSource;

    private String mUtmCreativeId;

    private String mUtmCreativeName;

    private String mUtmCountry;

    public ForYoadxInstallInfoBean() {
        mCnl = Config.Companion.getChannel();
    }

    public String getCnl() {
        return mCnl;
    }

    public void setCnl(String cnl) {
        mCnl = cnl;
    }

    public String getUtmSource() {
        return mUtmSource;
    }

    public void setUtmSource(String utmSource) {
        mUtmSource = utmSource;
    }

    public String getUtmCampaign() {
        return mUtmCampaign;
    }

    public String getmUtmCampaignNamem() {
        return mUtmCampaignNamem;
    }

    public String getmUtmMediumName() {
        return mUtmMediumName;
    }

    public void setmUtmMediumName(String mUtmMediumName) {
        this.mUtmMediumName = mUtmMediumName;
    }

    public String getmUtmMediumSource() {
        return mUtmMediumSource;
    }

    public void setmUtmMediumSource(String mUtmMediumSource) {
        this.mUtmMediumSource = mUtmMediumSource;
    }

    public String getmUtmCreativeName() {
        return mUtmCreativeName;
    }

    public void setmUtmCreativeName(String mUtmCreativeName) {
        this.mUtmCreativeName = mUtmCreativeName;
    }

    public void setmUtmCampaignNamem(String mUtmCampaignNamem) {
        this.mUtmCampaignNamem = mUtmCampaignNamem;
    }

    public void setUtmCampaign(String utmCampaign) {
        mUtmCampaign = utmCampaign;
    }

    public String getUtmContent() {
        return mUtmContent;
    }

    public void setUtmContent(String utmContent) {
        mUtmContent = utmContent;
    }

    public String getUtmMedium() {
        return mUtmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        mUtmMedium = utmMedium;
    }

    public String getUtmCreativeId() {
        return mUtmCreativeId;
    }

    public void setUtmCreativeId(String utmCreativeId) {
        mUtmCreativeId = utmCreativeId;
    }

    public String getUtmCountry() {
        return mUtmCountry;
    }

    public void setUtmCountry(String utmCountry) {
        mUtmCountry = utmCountry;
    }
}
