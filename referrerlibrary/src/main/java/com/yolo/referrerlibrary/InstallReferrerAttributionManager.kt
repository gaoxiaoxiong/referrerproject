package com.yolo.referrerlibrary

import android.app.Application
import com.blankj.utilcode.util.ProcessUtils
import com.blankj.utilcode.util.Utils
import com.yolo.base.installl.attributes.appsflyer.AppsFlyerReferrerClient
import com.yolo.base.installl.attributes.util.SaveInfoUtil
import com.yolo.cache.storage.YoloCacheStorage
import com.yolo.referrerlibrary.attribute.MergeAttribute
import com.yolo.referrerlibrary.attribute.google.GoogleReferrerClient
import com.yolo.referrerlibrary.constans.Config
import com.yolo.repository.http.core.YoloRepositoryConfig

/**
 * @date 创建时间: 2023/7/21
 * @auther gxx
 * @description 归因管理工具类
 **/
class InstallReferrerAttributionManager: MergeAttribute.OnIsReportServiceSuccessListener {
    private val TAG = "AttributesManager"
    private var mAppsFlyerReferrerClient: AppsFlyerReferrerClient? = null //appsFlyer
    private var mGoogleReferrerClient: GoogleReferrerClient? = null //google
    private var mApplication: Application? = null
    private var mMergeAttribute: MergeAttribute?=null//合并归因
    private var mAppsflyerKey: String = ""
    private var mAuthorizationHmacMd5Key: String = ""// hmac md5 key
    private var mAliasXTokenKey: Int = 0
    private var mAliasCommonXTokenKey: Int = 0
    private var mAliasFbRawKey: Int = 0
    private var mBusinessHost: String = ""//网络请求的地址
    private var mCommonHost: String = ""

    private constructor()

    class Build {
        private var mApplication: Application? = null
        private var mAppsflyerKey: String = ""

        private var mAuthorizationHmacMd5Key: String = ""// hmac md5 key
        private var mAliasXTokenKey: Int = 0
        private var mAliasCommonXTokenKey: Int = 0
        private var mAliasFbRawKey: Int = 0
        private var mBusinessHost: String = ""//网络请求的地址
        private var mCommonHost: String = ""

        fun getApplication(): Application {
            return mApplication!!
        }

        fun getAppsflyerKey(): String {
            return mAppsflyerKey
        }

        fun getCommonHost(): String {
            return mCommonHost
        }

        fun getBusinessHost(): String {
            return mBusinessHost
        }

        fun getAliasFbRawKey(): Int {
            return mAliasFbRawKey
        }

        fun getAliasCommonXTokenKey(): Int {
            return mAliasCommonXTokenKey
        }

        fun getAliasXTokenKey(): Int {
            return mAliasXTokenKey
        }

        fun getAuthorizationHmacMd5Key(): String {
            return mAuthorizationHmacMd5Key
        }

        fun setCommonHost(commonHost: String): Build {
            this.mCommonHost = commonHost
            return this
        }

        fun setBusinessHost(businessHost: String): Build {
            this.mBusinessHost = businessHost
            return this
        }

        fun setAliasFbRawKey(aliasFbRawKey: Int): Build {
            this.mAliasFbRawKey = aliasFbRawKey
            return this
        }

        fun setAliasCommonXTokenKey(aliasCommonXTokenKey: Int): Build {
            this.mAliasCommonXTokenKey = aliasCommonXTokenKey
            return this
        }

        fun setAliasXTokenKey(aliasXTokenKey: Int): Build {
            this.mAliasXTokenKey = aliasXTokenKey
            return this
        }

        fun setAuthorizationHmacMd5Key(authorizationHmacMd5Key: String): Build {
            this.mAuthorizationHmacMd5Key = authorizationHmacMd5Key
            return this
        }

        /**
         * @date 创建时间: 2023/7/21
         * @auther gxx
         * @description 设置Application
         **/
        fun setApplication(application: Application): Build {
            this.mApplication = application
            return this
        }

        /**
         * @date 创建时间: 2023/7/21
         * @auther gxx
         * @description 设置appsflyerKey
         **/
        fun setAppsflyerKey(appsflyerKey: String): Build {
            this.mAppsflyerKey = appsflyerKey
            return this
        }

        fun builder() {
            if (this.mApplication == null) {
                throw IllegalStateException("application is null")
            }

            if (mAuthorizationHmacMd5Key.isEmpty()) {
                throw IllegalStateException("authorizationHmacMd5Key is null")
            }

            if (mAliasXTokenKey <= 0) {
                throw IllegalStateException("mAliasXTokenKey<=0")
            }

            if (mAliasCommonXTokenKey <= 0) {
                throw IllegalStateException("mAliasCommonXTokenKey<=0")
            }

            if (mAliasFbRawKey <= 0) {
                throw IllegalStateException("mAliasFbRawKey<=0")
            }

            if (mBusinessHost.isEmpty()) {
                throw IllegalStateException("mBusinessHost is null")
            }

            if (mCommonHost.isEmpty()) {
                throw IllegalStateException("mCommonHost is null")
            }

            InstallReferrerAttributionManager().init(this)
        }
    }

    /**
     * @date 创建时间: 2023/7/20
     * @auther gxx
     * @description 初始化
     **/
    private fun init(build: Build) {
        this.mApplication = build.getApplication()
        this.mAppsflyerKey = build.getAppsflyerKey()
        this.mAuthorizationHmacMd5Key = build.getAuthorizationHmacMd5Key()
        this.mAliasXTokenKey = build.getAliasXTokenKey()
        this.mAliasCommonXTokenKey = build.getAliasCommonXTokenKey()
        this.mAliasFbRawKey = build.getAliasFbRawKey()
        this.mBusinessHost = build.getBusinessHost()
        this.mCommonHost = build.getCommonHost()

        //初始化yoloCache
        val currentProcessName = ProcessUtils.getCurrentProcessName()
        val packageName = Utils.getApp().packageName
        val isMainProcess = packageName.equals(currentProcessName)
        if (isMainProcess){
            YoloCacheStorage.init(mApplication)
        }

        //网络初始化  mAliasXTokenKey
        YoloRepositoryConfig.init(mAuthorizationHmacMd5Key,mBusinessHost,"",mAliasXTokenKey,mCommonHost,"",mAliasCommonXTokenKey)

        //配置 google  appsFlyer 初始化
        mGoogleReferrerClient = GoogleReferrerClient(build.getApplication())//google
        mAppsFlyerReferrerClient = AppsFlyerReferrerClient(mApplication!!, mAppsflyerKey, mGoogleReferrerClient!!)//appsFlyer
        mMergeAttribute = MergeAttribute(mGoogleReferrerClient!!,mAppsFlyerReferrerClient!!,mApplication!!,this)
        mGoogleReferrerClient?.setOnAttributesListener(mMergeAttribute!!)
        mAppsFlyerReferrerClient?.setOnAttributesListener(mMergeAttribute!!)

        if (!isReportServiceSuccess()) {
            val installInfoBean = SaveInfoUtil.getInstallInfoBeanFromLocal()
            if (installInfoBean != null) {
                mMergeAttribute?.saveInstallInfoToYoadxLib(installInfoBean)
                mMergeAttribute?.reportPlayStoreInstall(build.getApplication(), installInfoBean)
            }
        } else {
            val installInfoBean = SaveInfoUtil.getInstallInfoBeanFromLocal()
            if (installInfoBean != null) {
                mMergeAttribute?.saveInstallInfoToYoadxLib(installInfoBean)
            }
        }
    }

    /**
     * @date 创建时间: 2023/7/21
     * @auther gxx
     * @description 是否上报成功了  true 是的
     **/
    override fun isReportServiceSuccess(): Boolean {
        return YoloCacheStorage.getBoolean(
            Config.KEY_REPORT_INSTALL_CALL_SERVER_SUCCESSFUL,
            false
        )
    }


}