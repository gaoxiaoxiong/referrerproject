package com.yolo.referrerlibrary.network

import com.yolo.http.HttpUtilFactory
import com.yolo.referrerlibrary.bean.InstallCallResponse
import com.yolo.referrerlibrary.bean.InstallParamsRequest
import com.yolo.repository.http.core.YoloEncryptCommonHttpUtil
import com.yolo.repository.http.model.common.RequestContent
import com.yolo.repository.http.util.execute
import kotlinx.coroutines.flow.Flow

/**
  * @date 创建时间: 2023/7/21
  * @auther gxx
  * @description 网络请求处理
  **/
class RequestPresenter {

    /**
      * @date 创建时间: 2023/7/21
      * @auther gxx
      * @description 第一次的网络请求
      **/
    suspend fun readFirstOpen(bean:InstallParamsRequest): Flow<InstallCallResponse> {
        return HttpUtilFactory.create(YoloEncryptCommonHttpUtil::class.java)
            .getApi(ApiService::class.java)
            .readFirstOpen(RequestContent.createCommon(bean))
            .execute()
    }
}