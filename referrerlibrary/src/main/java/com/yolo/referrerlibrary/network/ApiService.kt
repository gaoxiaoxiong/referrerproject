package com.yolo.referrerlibrary.network

import com.yolo.referrerlibrary.bean.InstallCallResponse
import com.yolo.referrerlibrary.constans.Config
import com.yolo.repository.http.model.common.HttpResult
import kotlinx.coroutines.flow.Flow
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
    @POST(Config.API_NET_FIRST_OPEN)
    @JvmSuppressWildcards
    suspend  fun readFirstOpen(@Body params:Map<String,Any>): Flow<HttpResult<InstallCallResponse>>
}